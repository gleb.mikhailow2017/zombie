﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Craft : MonoBehaviour
{
    public int[] all;
    public GameObject inv;
    bool crafting;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision && collision.gameObject.tag == "craft")
        {
            crafting = true;
        }
        else
        {
            inv.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision && collision.gameObject.tag == "craft")
        {
            crafting = false;
            inv.SetActive(false);
        }
    }
    private void Update()
    {
        if (crafting)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                inv.SetActive(!inv.activeInHierarchy);
            }
        }
    }
    public void craft(int w,int m,int s, int n)
    {
        if (gameObject.GetComponent<GG>().Wd > w && gameObject.GetComponent<GG>().St > s)
        {
            gameObject.GetComponent<GG>().Wd -= w;
            gameObject.GetComponent<GG>().St -= s;
            all[n] += 1;
        }
    }
}
