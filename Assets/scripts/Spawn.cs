﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;
using UnityEngine.UI;

public class Spawn : MonoBehaviour
{
    public Transform[] Spawns;
    public GameObject Zomb1;
    public float SpawnT;
    public float i;
    public int wave;
    int tmp;

    void Start()
    {
        SpawnT = 6f;
    }

    void Update()
    {
        if (tmp > 0) 
        {
            if (i > 0)
            {
                i -= Time.deltaTime;
            }
            else
            {
                GameObject.Instantiate(Zomb1, Spawns[Random.Range(0,1)]);
                i = SpawnT;
                tmp--;
            }
        }
    }
    public void SpawnS(int ZombsCount, float spwnT)
    {
        wave = ZombsCount;
        SpawnT = spwnT;
        tmp = ZombsCount;
    }
}
