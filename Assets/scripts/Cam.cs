﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public GameObject go;
    void Update()
    {
        transform.position = new Vector3(go.transform.position.x, go.transform.position.y, transform.position.z);
    }
}
