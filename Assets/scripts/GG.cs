﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GG : MonoBehaviour
{
	public enum ProjectAxis { onlyX = 0, xAndY = 1 };
	public ProjectAxis projectAxis = ProjectAxis.onlyX;
	public float speed = 150;
	public float addForce;
	public bool lookAtCursor;
	public KeyCode leftButton = KeyCode.A;
	public KeyCode rightButton = KeyCode.D;
	public KeyCode upButton = KeyCode.W;
	public KeyCode downButton = KeyCode.S;
	public KeyCode addForceButton = KeyCode.Space;
	public bool isFacingRight = false;
	private Vector3 direction;
	private float vertical;
	private float horizontal;
	private Rigidbody2D body;
	private float rotationY;
	public bool jump = true;
	public int Hp;
	public Image txtHp;
	public Text GmOv;
	public int Wd = 0;
	public int St = 0;
	public Text WdDr;
	public Text StDr;

	public GameObject Turrel;

	void Start()
	{
		GmOv.enabled = false;
		body = GetComponent<Rigidbody2D>();

		if (projectAxis == ProjectAxis.xAndY)
		{
			body.gravityScale = 0;
			body.drag = 10;
		}
		WdDr.text = "";
		StDr.text = "";
	}
    void OnCollisionEnter2D(Collision2D coll)
	{
		jump = false;
		if(coll.gameObject.tag == "DrStone")
        {
			St++;
			StDr.text = St + " Камня";
			Destroy(coll.gameObject);
		}
		if (coll.gameObject.tag == "DrWood")
		{
			Wd++;
			WdDr.text = Wd + " Дерева";
			Destroy(coll.gameObject);
		}
	}

	void FixedUpdate()
	{
		txtHp.fillAmount = Hp / 20;
		StDr.text = St + " Камня";
		WdDr.text = Wd + " Дерева";
		body.AddForce(direction * body.mass * speed);

		if (Mathf.Abs(body.velocity.x) > speed / 100f)
		{
			body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * speed / 100f, body.velocity.y);
		}

		if (projectAxis == ProjectAxis.xAndY)
		{
			if (Mathf.Abs(body.velocity.y) > speed / 100f)
			{
				body.velocity = new Vector2(body.velocity.x, Mathf.Sign(body.velocity.y) * speed / 100f);
			}
		}
        if (Hp <= 0)
        {
            GmOv.enabled = true;
			Destroy(gameObject);
        }
	}

	void Flip()
	{
		if (projectAxis == ProjectAxis.onlyX)
		{
			isFacingRight = !isFacingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision && collision.gameObject.tag == "tur" && gameObject.GetComponent<Craft>().all[1] > 0)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
				GameObject b = GameObject.Instantiate(Turrel);
				b.transform.position = new Vector2(collision.transform.position.x, collision.transform.position.y + 1f);
				b.transform.SetParent(null);
				if (!isFacingRight) b.transform.localScale = new Vector3(-b.transform.localScale.x, b.transform.localScale.y);
				collision.gameObject.tag = "Untagged";
            }
        }

		if (collision && collision.gameObject.tag == "room")
		{
			if (Input.GetKeyDown(KeyCode.E))
			{
				transform.position = collision.gameObject.GetComponent<Room>().Second.transform.position;
			}
		}
	}

    void Update()
	{

		if (jump == false)
		{
			if (Input.GetKeyDown(KeyCode.W))
			{
				body.AddForce(gameObject.transform.up * addForce, ForceMode2D.Impulse);
				jump = true;
			}
		}
		if (lookAtCursor)
		{
			Vector3 lookPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
			lookPos = lookPos - transform.position;
			float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}

		if (Input.GetKey(upButton)) vertical = 1;
		else if (Input.GetKey(downButton)) vertical = -1; else vertical = 0;

		if (Input.GetKey(leftButton)) horizontal = -1;
		else if (Input.GetKey(rightButton)) horizontal = 1; else horizontal = 0;

		if (projectAxis == ProjectAxis.onlyX)
		{
			direction = new Vector2(horizontal, 0);
		}
		else
		{
			if (Input.GetKeyDown(addForceButton)) speed += addForce; else if (Input.GetKeyUp(addForceButton)) speed -= addForce;
			direction = new Vector2(horizontal, vertical);
		}

		if (horizontal > 0 && !isFacingRight) Flip(); else if (horizontal < 0 && isFacingRight) Flip();
	}
}
