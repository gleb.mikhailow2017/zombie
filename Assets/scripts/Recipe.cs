﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recipe : MonoBehaviour
{
    public GameObject pl;
    public int wood;
    public int stone;
    public int metal;
    public int id;
    public void Craft()
    {
        pl.GetComponent<Craft>().craft(wood, metal, stone, id);
    }
}
