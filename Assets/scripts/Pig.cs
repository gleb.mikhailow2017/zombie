﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Pig : MonoBehaviour
{
    public GameObject Meat;
    float HP;
    public float speed;
    Rigidbody2D Rg;

    public float tmp, Timer;
    float t = 1f;
    void Update()
    {
        if(tmp < Timer)
        {
            tmp += Time.deltaTime;
        }
        else
        {
            t = -t;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            int i = UnityEngine.Random.Range(0,7);
            Timer = i;
            tmp = 0;
        }
        Rg.velocity = new Vector2(speed * Time.deltaTime * t, 0f);
    }
    public void Awake()
    {
        Rg = GetComponent<Rigidbody2D>();
    }
    public void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.gameObject.tag == "Bullet")
        {
            Destroy(coll.collider.gameObject);
            HP -= 1f;
            GameObject.Instantiate(Meat, gameObject.transform);
            Destroy(gameObject);
            
        }
    }
}
