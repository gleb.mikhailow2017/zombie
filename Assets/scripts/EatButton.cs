﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EatButton : MonoBehaviour
{
    public Text f;

    public int HowMany;
    public float Energy;
    public GameObject Player;
    private void Start()
    {
        f = GetComponentInChildren<Text>();
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        f.text = HowMany.ToString();
    }
    public void Use()
    {
        Player.GetComponent<EatDrink>().UseFood(Energy);
        HowMany--;
        if (HowMany == 0)
        {
            transform.GetComponentInChildren<Image>().sprite = null;
        }
    }
}
