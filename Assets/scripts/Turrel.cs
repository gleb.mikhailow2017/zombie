﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrel : MonoBehaviour
{
    float tm = 0.5f;
    public GameObject bullet; 
    bool c;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision && collision.gameObject.tag == "Zom") { c = true; tm = 0.5f; }
        else c = false;
    }

    private void Update()
    {
        if (c)
        {
            if (tm > 0) tm -= Time.deltaTime;
            else
            {
                GameObject b = GameObject.Instantiate(bullet, gameObject.transform.GetChild(0).transform);
                b.GetComponent<Rigidbody2D>().AddForce(gameObject.transform.right * 1000f, ForceMode2D.Force);
                b.GetComponent<Timer>().TimeR = 2f;
                b.transform.SetParent(null);
            }
        }
    }
}
