﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public GameObject stone;
    public GameObject wood;
    float HP;
    float speed;
    Spawn sp;
    public GameObject Player;
    Rigidbody2D Rg;
    float TimeR ;
    bool Ty = false;

    int tmp;
    void Update()
    {
        if (HP<=0)
        {
            tmp = UnityEngine.Random.Range(1,3);
            if (tmp == 1)
            {
                GameObject drop = GameObject.Instantiate(stone, gameObject.transform);
                drop.transform.SetParent(null);
            }
            if(tmp == 2)
            {
                GameObject drop = GameObject.Instantiate(wood, gameObject.transform);
                drop.transform.SetParent(null);
            }
            Destroy(gameObject);
        }
        if (GetComponentInParent<Spawn>() != null)
            Rg.velocity = new Vector2(speed * Time.deltaTime, 0f);
        if (GetComponentInParent<Spawn>() == null)
            Rg.velocity = new Vector2(speed * Time.deltaTime * -1f, 0f);
    }
    public void OnCollisionStay2D(Collision2D col)
    {
        if(col.collider.gameObject.tag == "Player")
            Ty = true;
    }
    private void FixedUpdate()
    {
        if (Ty)
        {
            if (TimeR > 0)
                TimeR -= Time.deltaTime;
            else
            {
                Player.GetComponent<GG>().Hp--;
                TimeR = 1f;
            }
            Ty = false;
        }
    }
    public void Awake()
    {
        sp = GameObject.FindGameObjectsWithTag("Spawner")[0].GetComponent<Spawn>();
        Rg = GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectsWithTag("Player")[0];
        HP = sp.wave + 2f;
        speed = sp.wave + 40f;
    }
    public void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.gameObject.tag == "Bullet")
        {
            HP -= 1f;
            Destroy(coll.collider.gameObject);
        }
        if (coll.collider.gameObject.tag == "Player")
            Ty = true;
    }
}
