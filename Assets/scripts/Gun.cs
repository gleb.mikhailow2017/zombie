﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    GG g;
    public GameObject bullet;
    void Start()
    {
        g = GetComponentInParent<GG>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameObject b = GameObject.Instantiate(bullet, gameObject.transform);
            if(!g.isFacingRight)
            b.GetComponent<Rigidbody2D>().AddForce(-gameObject.transform.right *1000f,ForceMode2D.Force);
            else
            b.GetComponent<Rigidbody2D>().AddForce(gameObject.transform.right * 1000f, ForceMode2D.Force);
            b.GetComponent<Timer>().TimeR = 2f;
            b.transform.SetParent(null);
        }
    }
}
