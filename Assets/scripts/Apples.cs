﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apples : MonoBehaviour
{
    public Transform[] spawns;

    public int max;
    bool spawning = true;
    public GameObject apple;
    public float AplTime, tmp;

    private void Update()
    {
        if (spawning) {
            if (tmp < AplTime)
            {
                tmp += Time.deltaTime;
            }
            else
            {
                tmp = 0;
                GameObject b = GameObject.Instantiate(apple, spawns[Random.Range(0, 2)]);
                b.transform.localPosition = new Vector3(0, 0, 0);
            }
        }
        if (spawns[0].childCount > max || spawns[1].childCount > max || spawns[2].childCount > max) spawning = false;
        else spawning = true;
    }
}
