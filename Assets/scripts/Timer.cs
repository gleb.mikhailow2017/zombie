﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float TimeR;
    void Update()
    {
        if (TimeR > 0) 
            TimeR -= Time.deltaTime; 
        else 
            Destroy(gameObject);
    }
}
