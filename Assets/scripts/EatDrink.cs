﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EatDrink : MonoBehaviour
{
    public Image invDet;
    public GameObject inventory;
    int[] butindexes = new int[6];

    public Sprite Meat, Apple;
    public Image Eat, Drink;
    public float EatTime, DrinkTime, tmpEat, tmpDr;

    private void Start()
    {
        tmpDr = DrinkTime;
        tmpEat = EatTime;
        inventory.SetActive(false);
        invDet.enabled = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventory.SetActive(!inventory.activeInHierarchy);
            invDet.enabled = !invDet.enabled;
        }

        if (tmpDr > 0)
        {
            tmpDr -= Time.deltaTime;
            Drink.fillAmount = tmpDr / DrinkTime;
        }
        else
        {
            gameObject.GetComponent<GG>().Hp--;
            tmpDr = DrinkTime / 3;
        }

        if (tmpEat > 0)
        {
            tmpEat -= Time.deltaTime;
            Eat.fillAmount = tmpEat / EatTime;
        }
        else
        {
            gameObject.GetComponent<GG>().Hp--;
            tmpEat = EatTime / 3;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Apple")
        {
            for (int i = 0; i < 6; i++)
            {
                if (inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().HowMany == 0)
                {
                    butindexes[i] = 1;
                    inventory.transform.GetChild(i).gameObject.GetComponentInChildren<Image>().sprite = Apple;
                    inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().Energy = 50;
                    i = 6;
                    Destroy(collision.gameObject);
                }
                else
                {
                    if (butindexes[i] == 3)
                    {
                        inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().HowMany++;
                        Destroy(collision.gameObject);
                    }
                }
            }
        }
        if (collision.gameObject.tag == "Meat")
        {
            for (int i = 0; i < 6; i++)
            {

                if (inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().HowMany == 0)
                {
                    butindexes[i] = 2;
                    inventory.transform.GetChild(i).gameObject.GetComponentInChildren<Image>().sprite = Meat;
                    inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().Energy = 100;
                    i = 6;
                    Destroy(collision.gameObject);
                }
                else
                {
                    if (butindexes[i] == 2)
                    {
                        inventory.transform.GetChild(i).gameObject.GetComponent<EatButton>().HowMany++;
                        Destroy(collision.gameObject);
                    }
                }
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Water" && Input.GetKeyDown(KeyCode.E))
        {
            tmpDr = DrinkTime;
        }
    }

    public void UseFood(float energy)
    {
        if (tmpEat + energy <= EatTime)
            tmpEat += energy;
        else
            tmpEat = EatTime;
    }
}