﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour
{
    public Spawn sp;
    public float daySpeed, tmp;
    bool start;
    int zmb;

    private void Start()
    {
        zmb = 5;
        start = false;
    }
    private void Update()
    {
        if (tmp < daySpeed)
        {
            tmp += Time.deltaTime;
        }
        else
        {
            tmp = 0;
            start = false;
        }
        transform.rotation = Quaternion.Euler(0, 0, (tmp / daySpeed) * 360f);
        if(!start && tmp > daySpeed/2)
        {
            start = true;
            sp.SpawnS(zmb, 10f);
            zmb++;
        }
    }
}
